var http = require('http'), express = require('express'), app = express(), server = http
		.createServer(app), port = process.env.PORT || 5000, position = {
	x : 0,
	y : 0,
	z : 0,
	hand : '',
	present : false
};
var updated = false;

// var sleep = require('sleep');

server.listen(port, function() {
	console.log('Listening on ' + port);
});

function hasUpdate() {
	if (updated) {
		updated = false;
		return true;
	}
	return false;
}

var BinaryServer = require('binaryjs').BinaryServer;
var binaryServer = new BinaryServer({
	port : 9000

});
binaryServer.on('connection', function(client) {
	console.log('accept binary connection');
	var stream = client.createStream();
	var buffer = [];
	console.log('got stream');
	stream.on('data', function(data) {
		buffer.push(data);
		var text = buffer.join();
		var end = text.lastIndexOf('}');
		if (end > -1) {
			var temp = text.substr(0, end+1);
			var start = temp.lastIndexOf('{');
			if (start > -1) {
				var message = text.substr(start, end+1);

				var o=JSON.parse(message);
				updateHandPosition(o);
				
				if (end +1 < buffer.length) {
					buffer = buffer.splice(end + 1, buffer.length);
				}else{
					buffer=[];
				}
			}
//			console.log('received data from client ' + message + ' ' + end
//					+ ' ' + start + ' ');
			
		}
	});
});

app.use(express.bodyParser());

app.get('/', function(request, response) {
	response.sendfile('public/index.html');
});

function updateHandPosition(o){
	position.present = o.present;
	if (position.present) {
		position.x = o.x;
		position.y = o.y;
		position.z = o.z;
		position.hand = o.hand;
	} else {
		position.x = null;
		position.y = null;
		position.z = null;
		position.hand = null;
	}
	updated=true;
//	console.log('updating hand position '+JSON.stringify(position));
}

app.post('/update', function(request, response) {
	updateHandPosition(response.body);
	response.json({
		success : true
	});
	updated = true;
});

app.get('/retrieve', function(request, response) {
	response.json(getPositionData());
});

function getPositionData() {
	if (position.present) {
		return {
			present : true,
			x : position.x,
			y : position.y,
			z : position.z,
			hand : position.hand
		};
	} else {
		return {
			present : false
		};
	}
}

function forever(request, response) {
	this.response = response;
	this.request = request;

	var ref = this;
	this.request.on('close', function() {
		ref.clear();
	});
	this.request.on('end', function() {
		ref.clear();
	});
};

forever.prototype.clear = function() {
	this.request = null;
	this.response = null;
};
forever.prototype.stream = function() {
	if (this.response != null) {
		if (hasUpdate()) {
			this.response.write(JSON.stringify(getPositionData()));
			this.response.write(",");
		}
		var ref = this;
		setTimeout(function() {
			ref.stream();
		}, 5);
	}

};

app.get('/stream', function(request, response) {
	var f = new forever(request, response);
	f.stream();
});

app.get(/^(.+)$/, function(req, res) {
	res.sendfile('public/' + req.params[0]);
});
